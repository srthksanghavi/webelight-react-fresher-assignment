# Webelight React Fresher Assignment



## Practical Tasks

```
Task 1: Implement state management in a React component
Ask the candidate to create a simple React component that has a button and a text field.
When the button is clicked, the text in the text field should change.
This task will test the candidate's understanding of state management in React.
```

```
Task 2: Implement API integration in a React component
Ask the candidate to create a simple React component that displays a list of items obtained from an API. Pls use flex-box to make the page more interactive(design wise)
The API should return a JSON object with an array of items.
The React component should display the list of items on the screen.
This task will test the candidate's understanding of how to integrate APIs in React.
https://jsonplaceholder.typicode.com/guide/ Can be used.
Note : keep this above 2 task in single project, keep them under different route (you can use react-router-6)
```

```
Task 3: Debugging skills
Give the candidate a React component that has a bug.
Ask the candidate to debug the code and fix the issue.
This task will test the candidate's debugging skills.
Problem 1 : https://codesandbox.io/s/clever-http-zknlx1
Problem 2 : https://codesandbox.io/s/silly-carlos-t3tzhx
Note : For task 3 pls take the code from above link and make a new codesandbox link and share that link.
```

```
Task 4: Project task
Ask the candidate to build a simple project that incorporates all the skills tested in the previous tasks.
The project should have multiple components, state management, API integration, and should be responsive(use flex-box).
This task will test the candidate's ability to apply their skills to a real-world scenario.
Suggestion : you can use redux-toolkit, Material UI
```

